#!/usr/bin/env python

from distutils.core import setup

setup(name = "DSV",
      version = "1.4.1",
      description = "Delimiter-separated-values importer (aka CSV)",
      author = "Cliff Wells",
      author_email = "clifford.wells@comcast.net",
      url = "http://www.sf.net/projects/python-dsv/",
      license = "Python-style",
      packages = ["DSV"],
      ) 

